variable "aws_access_key" {}
variable "aws_secret_key" {}

provider "aws" {
  region = "us-west-2"
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
}

/*
terraform {
  backend "s3" {
      bucket = "ci-terraformstate-files"
      key    = "Wok/terraform.tfstate"
      region = "us-west-2"
       }
    }
    
*/

resource  "terraform_remote_state" "Wok" {
  provider = "aws"
  backend = "s3"
  config {
    bucket = "ci-terraformstate-files"
    key    = "Wok/terraform.tfstate"
    region = "us-west-2"
  }
}
    
    
resource "aws_volume_attachment" "ebs_att" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.example.id}"
  instance_id = "${aws_instance.web.id}"
}

resource "aws_instance" "web" {
  ami               = "ami-21f78e11"
  availability_zone = "us-west-2a"
  instance_type     = "t1.micro"

  tags {
    Name = "ImageoneHelloWorld"
  }
}

resource "aws_ebs_volume" "example" {
  availability_zone = "us-west-2a"
  size              = 1
  tags {
    Name = "ImageoneHelloWorld"
  }
}
