#Run this command to create intial backend for the terraform statefile.
#terraform remote config -backend=s3 -backend-config="bucket=HomerStateFile" -backend-config="key=VPC/terraform.tfstate" -backend-config="region=us-east-1" -backend-config="profile=uit"

#Configire your AWS credential with profile name
#prompt for profile name
variable "profile_name" {}


provider "aws" {
  region = "us-west-2"
  # Hashed for testing only. Need to unhash while pushing code to Gitlab or Terraform push
  #access_key = "${var.aws_access_key}"
  #secret_key = "${var.aws_secret_key}"
  profile = "${var.profile_name}"

}


#Create CloudTrail for the management events in new S3 bucket
resource "aws_cloudtrail" "aws_coreInfra_trail" {
  name                          = "coreinfra-trail-logs"
  s3_bucket_name                = "${aws_s3_bucket.coreinfra_cloudtrial.id}"
  s3_key_prefix                 = "logs/"
  include_global_service_events = true
}

#Creates S3 buckets for the Cloudtrial.
resource "aws_s3_bucket" "coreinfra_cloudtrial" {
    bucket = "coreinfra-cloudtrial-bucket"

    tags {
      Name        = "coreinfra-cloudtrial-bucket"
      Environment = "production"
    }

  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "AWSCloudTrailAclCheck",
            "Effect": "Allow",
            "Principal": {
              "Service": "cloudtrail.amazonaws.com"
            },
            "Action": "s3:GetBucketAcl",
            "Resource": "arn:aws:s3:::coreinfra-cloudtrial-bucket"
        },
        {
            "Sid": "AWSCloudTrailWrite",
            "Effect": "Allow",
            "Principal": {
              "Service": "cloudtrail.amazonaws.com"
            },
            "Action": "s3:PutObject",
            "Resource": "arn:aws:s3:::coreinfra-cloudtrial-bucket/*",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": "bucket-owner-full-control"
                }
            }
        }
    ]
}
POLICY
}

#=============================================================
# VPC creation with name tags "terraform-vpc-dev_2"
resource "aws_vpc" "terraform-vpc-dev" {
  cidr_block = "172.0.0.0/16"
  enable_dns_hostnames = true
  tags {
      Name = "Homer-vpc-001"
  }
}
#=============================================================
#Public Subnet
resource "aws_subnet" "us-west-2a-private" {
     vpc_id = "${aws_vpc.terraform-vpc-dev.id}"

     cidr_block = "172.0.0.0/20"
     availability_zone = "us-west-2a"

     tags {
          Name = "Homer-PublicSubnet-demo-001"
     }
 }

 resource "aws_route_table" "us-west-2a-private" {
     vpc_id = "${aws_vpc.terraform-vpc-dev.id}"

     route {
         cidr_block = "0.0.0.0/0"
         gateway_id = "${aws_internet_gateway.default.id}"
     }

     tags {
          Name = "Homer-Routetable-demo-001"
     }
 }

#Output the necessary variables.

 output "terraform_vpc_id" {
    value= "${aws_vpc.terraform-vpc-dev.id}"
  }

  output "terraform_subnet_id" {
    value= "${aws_subnet.us-west-2a-private.id}"

  }

#=================================================================
# Internet Gateway creation with the VPC ID
resource "aws_internet_gateway" "default" {
   vpc_id= "${aws_vpc.terraform-vpc-dev.id}"
   tags {
     Name = "Homer-IG-demo-001"
   }
 }
#==================================================================
#Create association between a subnet and routing table.

 resource "aws_route_table_association" "us-west-2a-private" {
     subnet_id = "${aws_subnet.us-west-2a-private.id}"
     route_table_id = "${aws_route_table.us-west-2a-private.id}"
 }

#==================================================================
# Create initial Secruity Groups
resource "aws_security_group" "ssh_http_sg" {
  name = "ssh_http_sg"
  description = "Allow SSH & http host"
  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  vpc_id = "${aws_vpc.terraform-vpc-dev.id}"
  tags {
      Name = "terraform_ssh_http_sg"
  }
}
output "ssh_http_sg_id" {
  value = "${aws_security_group.ssh_http_sg.id}"
}

#==================================================================

# Create new IAM group
resource "aws_iam_group" "group1" {
  name = "HOMER001"
  path = "/"
}

# Policy attachment using AdministratorAccess arn
resource "aws_iam_group_policy_attachment" "test-attach" {
  group      = "${aws_iam_group.group1.name}"
  policy_arn = "arn:aws:iam::aws:policy/AdministratorAccess"
}

# Create new User1
resource "aws_iam_user" "user1" {
    name = "HOMERuser001"
    path = "/"
}


# Create new User2
resource "aws_iam_user" "user2" {
    name = "HOMERuser002"
    path = "/"
}


# Attach user to groupname mygroup1
resource "aws_iam_group_membership" "team" {
  name = "HOMER001"

  users = [
    "${aws_iam_user.user1.name}",
    "${aws_iam_user.user2.name}",
  ]

  group = "HOMER001"
}

output "user1_arn" {
  value = "${aws_iam_user.user1.arn}"
}

output "user2_arn" {
  value = "${aws_iam_user.user2.arn}"
}

output "group_arn" {
  value = "${aws_iam_group.group1.arn}"
}
