provider "aws" {
  region = "us-west-2"
  profile = "uit"
}
/*
resource "aws_volume_attachment" "ebs_att" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.example.id}"
  instance_id = "${aws_instance.web.id}"
}

stanley

resource "aws_instance" "web" {
  ami               = "ami-21f78e11"
  availability_zone = "us-west-2a"
  instance_type     = "t1.micro"

  tags {
    Name = "ImageoneWorld"
  }
}

resource "aws_ebs_volume" "example" {
  availability_zone = "us-west-2a"
  size              = 1
  tags {
    Name = "ImageoneWorld"
  }
}

*/

resource "aws_launch_configuration" "agents-ag" {
  name_prefix   = "terraform-lc-example-"
  image_id      = "ami-44bRSecurt3d"
  instance_type = "t2.micro"
  key_name = "CoreInfra1"

  lifecycle {
    create_before_destroy = true
  }

  root_block_device{
 		volume_type = "gp2"
		volume_size = "10"
	}

}

#VPC

resource "aws_vpc" "terraform-vpc-dev" {
  cidr_block = "172.0.0.0/16"
  enable_dns_hostnames = true
  tags {
      Name = "terraform-vpc-dev_2"
  }
}

 resource "aws_internet_gateway" "default" {
   vpc_id= "${aws_vpc.terraform-vpc-dev.id}"
 }
/*
   Public Subnet
*/
 resource "aws_subnet" "us-west-2b-private" {
     vpc_id = "${aws_vpc.terraform-vpc-dev.id}"

     cidr_block = "172.0.0.0/20"
     availability_zone = "us-west-2b"

     tags {
         Name = "Private terraform Subnet 2b"
     }
 }

 resource "aws_route_table" "us-west-2b-private" {
     vpc_id = "${aws_vpc.terraform-vpc-dev.id}"

     route {
         cidr_block = "0.0.0.0/0"
         gateway_id = "${aws_internet_gateway.default.id}"
     }

     tags {
         Name = "Private terraform Subnet 2"
     }
 }

 resource "aws_route_table_association" "us-west-2b-private" {
     subnet_id = "${aws_subnet.us-west-2b-private.id}"
     route_table_id = "${aws_route_table.us-west-2b-private.id}"
 }


#SG
 resource "aws_security_group" "ssh_http_sg" {
   name = "ssh_http_sg_2"
   description = "Allow SSH & http host"
   ingress {
     from_port = 22
     to_port = 22
     protocol = "tcp"
     cidr_blocks = ["172.0.0.0/16"]
   }
   ingress {
     from_port = 80
     to_port = 80
     protocol = "tcp"
     cidr_blocks = ["172.0.0.0/16"]
   }

   egress {
     from_port = 0
     to_port = 0
     protocol = "-1"
     cidr_blocks = ["172.0.0.0/16"]
   }
   vpc_id = "${aws_vpc.terraform-vpc-dev.id}"
   tags {
       Name = "terraform_ssh_http_sg_2"
   }
 }

 output "ssh_http_sg_id" {
   value = "${aws_security_group.ssh_http_sg.id}"
 }

 output "terraform_vpc_id" {
   value= "${aws_vpc.terraform-vpc-dev.id}"
 }

 output "terraform_subnet_id" {
   value= "${aws_subnet.us-west-2b-private.id}"

 }


resource "aws_autoscaling_group" "agents" {
    availability_zones = ["us-west-2b"]
    name                 = "terraform-agents"
    launch_configuration = "${aws_launch_configuration.agents-ag.name}"
    min_size             = 1
    max_size             = 2
    vpc_zone_identifier = ["${aws_subnet.us-west-2b-private.id}"]

    tag {
        key = "Name"
        value = "ASB Node"
        propagate_at_launch = true
    }

    lifecycle {
      create_before_destroy = true
    }

}

resource "aws_autoscaling_policy" "agents-scale-up" {
    name = "agents-scale-up"
    scaling_adjustment = 1
    adjustment_type = "ChangeInCapacity"
    cooldown = 300
    autoscaling_group_name = "${aws_autoscaling_group.agents.name}"
}

resource "aws_autoscaling_policy" "agents-scale-down" {
    name = "agents-scale-down"
    scaling_adjustment = -1
    adjustment_type = "ChangeInCapacity"
    cooldown = 300
    autoscaling_group_name = "${aws_autoscaling_group.agents.name}"
}

resource "aws_cloudwatch_metric_alarm" "cpu-high" {
    alarm_name = "cpu-util-high-agents"
    comparison_operator = "GreaterThanOrEqualToThreshold"
    evaluation_periods = "2"
    metric_name = "CPUUtilization"
    namespace = "System/Linux"
    period = "300"
    statistic = "Average"
    threshold = "60"
    alarm_description = "This metric monitors ec2 memory for high utilization on agent hosts"
    alarm_actions = ["${aws_autoscaling_policy.agents-scale-up.arn}"]
    treat_missing_data= "breaching"
    dimensions {
        AutoScalingGroupName = "${aws_autoscaling_group.agents.name}"
    }
}

resource "aws_cloudwatch_metric_alarm" "cpu-low" {
    alarm_name = "cpu-util-low-agents"
    comparison_operator = "LessThanOrEqualToThreshold"
    evaluation_periods = "2"
    metric_name = "CPUUtilization"
    namespace = "System/Linux"
    period = "300"
    statistic = "Average"
    threshold = "40"
    alarm_description = "This metric monitors ec2 memory for low utilization on agent hosts"
    alarm_actions = ["${aws_autoscaling_policy.agents-scale-down.arn}"]
    treat_missing_data= "breaching"
    dimensions {
        AutoScalingGroupName = "${aws_autoscaling_group.agents.name}"
    }
}
